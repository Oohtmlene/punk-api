import React from 'react';
import { Header, Footer } from './layout';
import PunkBrowser from './modules/PunkBrowser';
import './app.css';

const App = () => (
  <div className="page">
    <Header />
    <main className="content">
      <PunkBrowser />
    </main>
    <Footer />
  </div>
);

export default App;
