import React from 'react';
import { func } from 'prop-types';
import Select from '../../forms/Select';

const AbvFilter = ({ onFilterChange }) => {
  const options = [
    {
      key: 'all',
      name: 'all',
      value: 'abv_gt=0&abv_lt=12',
    },
    {
      key: '0-5',
      name: '0 to 5%',
      value: 'abv_gt=0&abv_lt=4',

    },
    {
      key: '5-8',
      name: '5 to 8%',
      value: 'abv_gt=5&abv_lt=8',
    },
    {
      key: '8-12',
      name: '8 to 12%',
      value: 'abv_gt=8&abv_lt=12',
    },
  ];

  return (
    <Select
      name="abv"
      placeholder="filter by ABV"
      options={options}
      handleChange={(event, value) => onFilterChange(event, value)}
    />
  );
};

AbvFilter.propTypes = {
  onFilterChange: func.isRequired,
};

AbvFilter.defaultProps = {};

export default AbvFilter;
