import React from 'react';
import {
  arrayOf,
  func,
  object,
  string,
} from 'prop-types';
import { toKey } from '../../../helpers';
import './select.css';


const Select = (props) => {
  const handleChange = (e) => {
    props.handleChange(props.name, e.target.value);
  };
  return (
    <select
      className="select"
      name={props.name}
      onChange={handleChange}
    >
      <option className="placeholder">Sort by ABV</option>
      {props.options.map(option => (
        <option
          key={toKey(option.name)}
          value={option.value}
        >
          {option.name}
        </option>
      ))}
    </select>
  );
};

Select.propTypes = {
  options: arrayOf(object).isRequired,
  name: string.isRequired,
  handleChange: func,
};

Select.defaultProps = {
  handleChange: () => { },
};

export default Select;

