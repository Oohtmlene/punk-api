import React from 'react';
import { } from 'prop-types';
import './footer.css';

const Footer = () => <footer className="footer">Punk API demo - @htmlene</footer>;

Footer.propTypes = {};
Footer.defaultProps = {};

export default Footer;
