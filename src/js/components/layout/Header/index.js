import React from 'react';
import {} from 'prop-types';
import './header.css';

const Header = () => (
  <header className="header">
    <h1 className="header__title h2">The Punk IPA brewery</h1>
  </header>
);

Header.propTypes = {};
Header.defaultProps = {};

export default Header;
