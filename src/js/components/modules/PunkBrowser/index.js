import React, { Component, Fragment } from 'react';
import { } from 'prop-types';
import { fetching } from '../../../helpers';
import { ProductList, ProductEntry } from '../../views';
import AbvFilter from '../../filters/AbvFilter';

class PunkBrowser extends Component {
  constructor() {
    super();
    this.state = {
      hits: [],
      hit: {},
      modal: false,
      isLoading: true,
      params: '',
    };
  }

  componentDidMount() {
    fetching(process.env.REACT_APP_PUNK_API)
      .then((hits) => {
        this.setState({ hits, isLoading: false });
      });
  }

  // FILTER
  onFilterChange = (event, value) => {
    const params = value;
    this.setState({ params, isLoading: true }, () => {
      // fetch the data again
      fetching(`${process.env.REACT_APP_PUNK_API}?${this.state.params}`)
        .then((hits) => {
          this.setState({ hits, isLoading: false });
        });
    });
    // TODO - Make it flexible using URLSearchParams
    // const searchParams = new URLSearchParams(paramsString);
    // for (const p of searchParams) {
    //   => update searchParams object state
    // }
  }

  // PRODUCT ENTRY
  openModal = (id) => {
    const hit = this.findHitByid(this.state.hits, 'id', id);
    this.setState({ hit, modal: true });
  };

  findHitByid = (arr, key, id) => this.state.hits.filter(hit => hit[key] === id)[0];

  closeModal = () => {
    this.setState({ modal: false });
  }

  render() {
    const {
      hit,
      hits,
      isLoading,
      modal,
    } = this.state;

    return (
      <Fragment>
        <AbvFilter
          onFilterChange={this.onFilterChange}
        />
        <ProductList
          handleOpen={this.openModal}
          hits={hits}
          isLoading={isLoading}
        />
        {modal &&
          <ProductEntry
            hit={hit}
            isLoading={isLoading}
            handleClose={this.closeModal}
          />
        }
      </Fragment >
    );
  }
}

export default PunkBrowser;
