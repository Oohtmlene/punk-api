import React from 'react';
import { func, string, oneOf } from 'prop-types';
import './button.css';

const ButtonLink = props => (
  <a
    className={`button button--${props.className}`}
    href={props.href}
    title={props.text}
    target={props.target}
    rel={props.rel}
    onClick={props.onClick}
  >
    {props.text}
  </a>
);


ButtonLink.propTypes = {
  className: oneOf(['primary', 'secondary', 'tertiary']),
  text: string,
  href: string,
  target: string,
  rel: string,
  onClick: func,
};

ButtonLink.defaultProps = {
  className: 'primary',
  text: 'Send',
  href: '#',
  target: '_self',
  rel: '',
  onClick: () => { },
};

export default ButtonLink;
