import React from 'react';
import { string } from 'prop-types';

//= I can change that to SVG
const FontIcon = ({ icon }) => <i className={`fas fa-${icon} icon`} />;

FontIcon.propTypes = {
  icon: string.isRequired,
};

FontIcon.defaultProps = {};

export default FontIcon;
