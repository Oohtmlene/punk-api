import React from 'react';
import { string } from 'prop-types';
import './loader.css';

const Loader = ({ text }) => <span className="is-loading">{text}</span>;

Loader.propTypes = {
  text: string,
};

Loader.defaultProps = {
  text: 'The content is loading...',
};

export default Loader;
