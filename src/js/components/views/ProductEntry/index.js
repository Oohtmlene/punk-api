import React from 'react';
import { any, bool, func, objectOf } from 'prop-types';
import { toKey } from '../../../helpers';
import { FontIcon } from '../../patterns';
import './product-entry.css';

const ProductEntry = ({ hit, isLoading, handleClose }) => {
  if (isLoading) {
    return false;
  }
  const bgImg = {
    backgroundImage: `url(${hit.image_url})`,
  };

  return (
    <div className="entry entry--product">
      <div className="entry__inner">
        <article className="entry__content">
          <header className="entry__header">
            <h2 className="entry__title h3">{hit.name}</h2>
            <p className="entry__tagline h4">{hit.tagline}</p>
          </header>
          <main className="entry__main">
            <p className="entry__description">{hit.description}</p>
          </main>
          <aside className="entry__details">
            <h3 className="entry__details__title h4">Brew Sheet</h3>
            <ul className="entry__list">
              <li className="entry__list__item">Abv: {hit.abv}</li>
              <li className="entry__list__item">Ibu: {hit.ibu}</li>
              <li className="entry__list__item">Ph: {hit.ph}</li>
              <li className="entry__list__item">Og: {hit.target_og}</li>
            </ul>
            <h3 className="entry__details__title h4">Perfect with:</h3>
            <ul className="entry__list">
              {hit.food_pairing.map(item => <li key={toKey(item)}className="entry__list__item">{item}</li>)}
            </ul>
          </aside>
        </article>
        <figure className="entry__figure" style={bgImg}>
          <img className="entry__image" src={hit.image_url} alt="hit.name" />
        </figure>
        <button className="entry__close" onClick={handleClose}><FontIcon icon="times" />  Close</button>
      </div>
    </div>
  );
};

ProductEntry.propTypes = {
  hit: objectOf(any).isRequired,
  isLoading: bool.isRequired,
  handleClose: func.isRequired,
};

ProductEntry.defaultProps = {};

export default ProductEntry;
