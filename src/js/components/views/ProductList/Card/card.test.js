import React from 'react';
import renderer from 'react-test-renderer';
import Card from './';

describe('Card component', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(<Card
        handleOpen={() => {}}
        image_url="#"
        name="Product name"
        tagline="Product tagline"
        abv={4}
      />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
