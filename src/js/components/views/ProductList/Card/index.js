import React from 'react';
import { func, number, string } from 'prop-types';
import { truncate } from '../../../../helpers';
import { FontIcon, ButtonLink } from '../../../patterns';
import './card.css';

const Card = (props) => {
  const bgImg = {
    backgroundImage: `url(${props.image_url})`,
  };
  return (
    <li className="card">
      <figure className="card__figure" style={bgImg}>
        <span
          role="button"
          tabIndex="-1"
          onClick={props.handleOpen}
          onKeyPress={props.handleOpen}
          className="card__link"
        >
          <img
            className="card__image"
            src={props.image_url}
            alt={props.name}
          />
        </span>
      </figure>
      <header className="card__description">
        <h2 className="card__title h4">{props.name}</h2>
        <p className="card__tagline">{truncate(props.tagline, 60)}</p>
        <p className="card__abv">
          {props.abv} <FontIcon icon="percentage" />
        </p>
      </header>
      <div className="card__cta">
        <ButtonLink
          text="More details"
          onClick={props.handleOpen}
        />
      </div>
    </li>
  );
};

Card.propTypes = {
  handleOpen: func.isRequired,
  image_url: string.isRequired,
  name: string.isRequired,
  tagline: string.isRequired,
  abv: number.isRequired,
};

Card.defaultProps = {};

export default Card;
