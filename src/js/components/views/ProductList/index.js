import React, { Fragment } from 'react';
import { arrayOf, bool, object } from 'prop-types';
import { toKey } from '../../../helpers';
import { Loader } from '../../patterns';
import Card from './Card';
import './product-list.css';

const ProductList = (props) => {
  const item = props.hits.map(hit => (
    <Card
      key={toKey(hit.name)}
      handleOpen={() => props.handleOpen(hit.id, hit.name)}
      {...hit}
    />
  ));

  return (
    <Fragment>
      <ul className="list list--product">
        { props.isLoading ?
          <Loader
            text="Patience, the beers are coming"
          />
          :
          item
        }
      </ul>
    </Fragment>
  );
};

ProductList.propTypes = {
  isLoading: bool.isRequired,
  hits: arrayOf(object).isRequired,
};

ProductList.defaultProps = {};

export default ProductList;
