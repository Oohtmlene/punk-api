import React from 'react';
import { toKey, toClass } from './';

describe('Helpers functions', () => {
  describe('toKey function', () => {
    describe('toKey(string)', () => {
      it('String should return a lower case attached string', () => {
        const string = 'Hello World';
        expect(toKey(string)).toBe('helloworld');
      });
    });
    describe('toKey(number)', () => {
      it('Number should return a lower case attached string', () => {
        const number = 45454;
        expect(toKey(number)).toBe('45454');
      });
    });
    describe('toKey(array)', () => {
      it('Array should return a lower case attached string', () => {
        const array = [1, 2, 3];
        expect(toKey(array)).toBe('123');
      });
    });
    describe('toKey(boolean)', () => {
      it('Boolean should return a lower case attached string', () => {
        const boolean = true;
        expect(toKey(boolean)).toBe('true');
      });
    });
  });
  describe('toClass function', () => {
    describe('toClass(string)', () => {
      it('String should return a lower case dashed string', () => {
        const string = 'Hello World';
        expect(toClass(string)).toBe('hello-world');
      });
    });
    describe('toClass(number)', () => {
      it('Number should return a lower case string', () => {
        const number = 45454;
        expect(toClass(number)).toBe('45454');
      });
    });
    describe('toClass(array)', () => {
      it('Array should return a lower case dashed string', () => {
        const array = [1, 2, 3];
        expect(toClass(array)).toBe('1-2-3');
      });
    });
    describe('toClass(boolean)', () => {
      it('Boolean should return a lower case string', () => {
        const boolean = true;
        expect(toClass(boolean)).toBe('true');
      });
    });
  });
});
