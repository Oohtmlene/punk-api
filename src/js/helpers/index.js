import 'whatwg-fetch';

export function toKey(el) {
  return el.toString().toLowerCase().replace(/[,\s]+|[,\s]+/g, '');
}

export function toClass(el) {
  return el.toString().toLowerCase().replace(/[,\s]+|[,\s]+/g, '-');
}

export function truncate(el, n) {
  return `${el.toString().substring(0, n)}...`;
}

// Fetching data
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    const json = response.json();
    return json;
  }
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

export function fetching(url, options) {
  return fetch(url, options).then(checkStatus);
}
