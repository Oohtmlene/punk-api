Pure SASS-adaption of Lea Verou's [contrast-ratio javascript](https://github.com/LeaVerou/contrast-ratio). Can be useful when eg. generating colored buttons from a single supplied color as you can then check which out of a couple of text colors would give the best contrast.

This script currently lacks the support for alpha-transparency that Lea supports in her script though.

In addition to the color-contrast adaption there's also some math methods that were needed to be able to calculate the exponent of a number and especially so when the exponent is a decimal number. A 2.4 exponent is used to calculate the luminance of a color and calculating such a thing is not something that SASS supports out of the box and not something I found a good pure-SASS script for calculating and I much prefer pure-SASS over ruby extensions. The math methods might perhaps be unecessary though if you're running Compass or similar as they may provide compatible math methods themselves.

_Normal usage_: `color: pick_best_color(#f00, (#fff, #ccc, #666));`

_Bonus feature_: Just want to get warned when the contrast becomes unacceptably low? Supply just that one color in the color-pick function: `color: pick_best_color(#f00, #fff);`

_Math methods_: `pow(10, 3.14)` and `nthRoot(32, 5)`